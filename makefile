OPENCV= -lopencv_core -lopencv_highgui -lopencv_features2d -lopencv_calib3d -lopencv_legacy
FLAGS= -Wall -g
CFLAGS = `pkg-config --cflags opencv`
LIBS = `pkg-config --libs opencv`
OBJECTS = MKalman.o \
	 Tracker.o \
	TrackObj.o \
	people_counter.o \
	geometrylib.o

exec: $(OBJECTS)
	g++ $(CFLAGS) $(FLAGS) -o exec $(OBJECTS) $(LIBS)

people_counter.o: people_counter.cpp
	g++ $(FLAGS) -I./dlib/ -c people_counter.cpp

MKalman.o:MKalman.cpp MKalman.h
	g++ $(FLAGS) -c MKalman.cpp

Tracker.o: Tracker.cpp Tracker.h TrackObj.h
	g++  $(FLAGS) -I./dlib/ ./dlib/dlib/all/source.cpp -c Tracker.cpp

TackObj.o: TrackObj.cpp TrackObj.h
	g++ $(FLAGS)  -c TrackObj.cpp

geometrylib.o: geometrylib.cpp geometrylib.h
	g++ $(FLAGS) -c geometrylib.cpp

clean:
	rm *~ *.o exec
