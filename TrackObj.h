/*Authors:
  -Gervs
  -Vito
*/

#ifndef TRACKOBJ_H
#define TRACKOBJ_H

#include "opencv2/core/core.hpp"
#include "MKalman.h"


using namespace cv;

class TrackObj{
 
 public:
  vector<Point2f> history; //Points the object has been in the previous frames
  Scalar color; //color in which the objectcs trajectories will be drawn

  bool wentRight = false;
  bool wentLeft = false;


 TrackObj(Scalar colorA, Point2f iniP):
  color(colorA),
    filter(iniP.x, iniP.y),
    wentRight(false),
    wentLeft(false)
      {nFramesUnseen = 0;
	history.push_back(iniP);}
    
  void predict();
  void correct(Point2f p);
  int framesUnseen();
  Point2f lastHistory();

 private:
  MKalman filter; //Each tracked object is associated with a Kalman FIlter
  int nFramesUnseen; //For how many frames the object hasnt been detected
 
};


#endif
