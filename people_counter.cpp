#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/video/background_segm.hpp"

#include "geometrylib.h"
#include "MKalman.h"
#include "TrackObj.h"
#include "Tracker.h"

using namespace cv;
using namespace std;

Point *crossing = new Point[2];
int selectedPoints = 0;
Mat firstFrame;

void mouse_click(int event, int x, int y, int flags, void *param);

int main(int argc, char ** argv){
  VideoCapture scene;
  int key = 1;
  Tracker tracker;
  int numFrames = 0;
  int peopleCountLeft = 0;
  int peopleCountRight = 0;
  if(argc >= 2){
    scene = VideoCapture(argv[1]);
  }
  else  scene = VideoCapture(0);
  
  if(!scene.isOpened() ){
    cout << " --(!) Error opening video." << endl;
    exit(-2); 
  }
  
  scene >> firstFrame;

  namedWindow("Original", WINDOW_NORMAL );

  cvSetMouseCallback("Original", mouse_click, 0);
  
  imshow("Original", firstFrame);
  cout << "Select two points to define a line." << endl;
  while(selectedPoints < 2)
    key = waitKey(1);
  

  SimpleBlobDetector dec;

  BackgroundSubtractorMOG2 bgDetec(300,20,true);
  bgDetec.set("nmixtures", 3); // set number of gaussian mixtures
  // bgDetec.set("bShadowDetection",false); // turn the shadow detection off
 
  vector< vector<Point> > contours, contours2;
  vector< vector<Point> > blobContours;
  vector<KeyPoint> keyPoints;
  
  while(key != 27){
    Mat scenecolor, scene_gray, foreground, bgImg;
    vector<KeyPoint> keypoints;
    scene >> scenecolor;
    if(!scenecolor.data) break;
    cvtColor(scenecolor, scene_gray, CV_BGR2GRAY);
    
    bgDetec.operator ()(scene_gray,foreground); //gets foreground image

    bgDetec.getBackgroundImage(bgImg); //gets background image

    //Remove shadows
    threshold(foreground, foreground, 200, 255, THRESH_BINARY);

    //Test - median filter
    //    medianBlur(foreground, foreground, 3);

    // Test
    findContours(foreground, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    drawContours(foreground, contours, -1, Scalar(255,0,0), CV_FILLED);
    contours.clear();
    // End test
    
    // imshow("After fill", foreground);

    Mat morphOpening(7,7,CV_8U,cv::Scalar(1));
    Mat morphClosing(7,7,CV_8U,cv::Scalar(1));

        //opening
    erode(foreground,foreground,morphOpening);
    dilate(foreground,foreground,morphOpening);

    //closing
    
    dilate(foreground,foreground,morphClosing);
    erode(foreground,foreground,morphClosing);

    dilate(foreground,foreground,morphOpening);
    dilate(foreground,foreground,morphOpening);

    /*
    findContours(foreground, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    drawContours(foreground, contours, -1, Scalar(255,0,0), CV_FILLED);
    contours.clear();
    
    imshow("Foreground", foreground);*/


    dec.detect(foreground, keyPoints, cv::Mat()); //Blob detector


    findContours(foreground, contours2, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    
    vector<vector<Point> >::const_iterator it = contours2.begin();
    vector<vector<Point> >::const_iterator end = contours2.end();
    int minArea = 700;

    // Delete small contours
    while (it != end) {
      if(contourArea(*it) >= minArea)
	contours.push_back(*it);
      ++it;
    }

    //drawContours(scenecolor, contours2, -1, Scalar(255,0,0), 2);

    it = contours.begin();
    end = contours.end();
    
    while (it != end) {
      Rect bounds = boundingRect(*it);
      rectangle(scenecolor, bounds, Scalar(0,255,0), 2);
      ++it;
    }

    line(scenecolor, crossing[0], crossing[1], Scalar(153, 0, 76), 4);

    //Find centroids

    /// Get the moments


    vector<Moments> mu(contours.size() );

    for(unsigned int i = 0; i < contours.size(); i++ ){ 
      //  printf("Area: %.2f\n", contourArea(contours[i])); 
      mu[i] = moments( contours[i], false ); 
    }

    ///  Get the mass centers:
    vector<Point2f> mc( contours.size() );
    for(unsigned int i = 0; i < contours.size(); i++ )
      { mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); }
    for(unsigned int i = 0; i < mu.size(); i++)
      circle( scenecolor, mc[i], 4, Scalar(0,0,255), -1, 8, 0 );

    if(!tracker.wasInitialized()){
      tracker.init(mc);
      continue;
    }

    tracker.track(mc);
    //Draw line history[0] -> history[1] for every object
    for(unsigned int i = 0; i < tracker.objects.size(); i++)
      {
	vector<Point2f> h = tracker.objects[i].history;
	if(h.size() == 1) circle( scenecolor, h[h.size()-1], 4, Scalar(144, 32, 208), -1, 8, 0 );
	else{
	  for(unsigned int j = 1; j < h.size(); j++){
	    line( scenecolor, h[j-1], h[j], tracker.objects[i].color, 2, 0);
	    
	  }
	  if(intersects(crossing[0],crossing[1], h[h.size()-1], h[h.size()-2]))
	    if(left(crossing[0],crossing[1], h[h.size()-1]) != 
	       left(crossing[0],crossing[1], h[h.size()-2]))
	      {
		if(left(crossing[0],crossing[1], h[h.size()-1]) &&
		   !tracker.objects[i].wentLeft) // Don't repeat the count
		  { peopleCountLeft++;tracker.objects[i].wentLeft = true;}
		if(!left(crossing[0],crossing[1], h[h.size()-1]) &&
		   !tracker.objects[i].wentRight) // Don't repeat the count
		  {peopleCountRight++;tracker.objects[i].wentRight = false;}
	      }
	}
      }

    stringstream ss,ss2;
    ss << peopleCountLeft;
    ss2 << peopleCountRight;
      String msg = "L:"+ ss.str() + " | R:"+ ss2.str(); 
    putText(scenecolor, msg, Point(0,scenecolor.rows), FONT_HERSHEY_SIMPLEX,
	    2, Scalar(255,255,0), 5, 8 );
    						     
    // imshow("Background", bgImg);
    imshow("Original", scenecolor);

    namedWindow("After morph", WINDOW_NORMAL );
    imshow("After morph", foreground);

    
    namedWindow("Background subtraction", WINDOW_NORMAL );
    imshow("Background subtraction", foreground);
  
    // Imshow ("Foreground", Foreground);
    //usleep(500000);
     

    key = waitKey();
  }
}

void mouse_click(int event, int x, int y, int flags, void *param){
  Mat sel;
  switch(event){
  case CV_EVENT_LBUTTONDOWN:
    cout << "Mouse pressed" << endl;
    if(selectedPoints < 2){
      sel = firstFrame.clone();
      crossing[selectedPoints].x = x;
      crossing[selectedPoints].y = y;
      circle(sel, crossing[selectedPoints++], 5, Scalar(153, 0, 76), -1);
      if(selectedPoints == 2){
	line(sel, crossing[0], crossing[1], Scalar(153, 0, 76), 4);
      }
      imshow("Original", sel);
    } else{
      cout << "Line already selected." << endl;
    }
    break;
  default:
    break;
  }
}


