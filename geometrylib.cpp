
#include "geometrylib.h"


using namespace cv;

/*
 * "Returns 2 times the signed area of the triangle determined by 
 * the points a,b and c"
 */

double area2 (Point2f a, Point2f b, Point2f c)
{
  return (b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x);
}


/*
 * True if c is in the left of the segment ab
 */

bool left(Point2f a, Point2f  b, Point2f c)
{
  return area2(a, b, c) > 0;
}


/*
 * True if a, b and c are colinear points
 */
bool collinear(Point2f a, Point2f b, Point2f c)
{
  return area2(a, b, c) == 0;
}

/*
 * Returns true if the Point2f c is in the segment ab 
 */

bool between(Point2f a, Point2f b, Point2f c)
{
  if(!collinear(a,b,c)) return false;
  if(a.x != b.x)
    return ((a.x <= c.x && c.x <= b.x) ||
	    (b.x <= c.x && c.x <= a.x));
  else
    return ((a.y <= c.y && c.y <= b.y) ||
	    (b.y <= c.y && c.y <= a.y));
}

/*
 * Returns true if the segment ab intersects the segment 
 * cd properly
 */

bool intersectsProp(Point2f a, Point2f b, 
		   Point2f c, Point2f d)
{
  if(collinear(a,b,c) || collinear(a,b,d) ||
     collinear(c,d,a) || collinear(c,d,b))
    return false;
  else
    return ( (left(a,b,c) ^ left(a,b,d)) &&
	     (left(c,d,a) ^ left(c,d,b)) );
}

/*
 * Returns true if the segment ab intersects the segment 
 * cd properly
 */

bool intersects(Point2f a, Point2f b, Point2f c, Point2f d)
{
  if(intersectsProp(a, b, c, d)) return true;
  return (between(a,b, c) || between(a,b, d) ||
	  between(c, d, a) || between(c, d, b));
}
