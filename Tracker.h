/*Authors:
  -Gervs
  -Vito
*/

#ifndef TRACKER_H
#define TRACKER_H


#include "MKalman.h"
#include "TrackObj.h"
#include <dlib/matrix.h>

using namespace cv;

class Tracker{
  
  bool initialized = false;

 public:
  vector<TrackObj> objects;
  Tracker();
  void track(vector<Point2f> measurements);
  void init(vector<Point2f> measurements);
  bool wasInitialized();

 private:
  dlib::matrix<int> distance_matrix(vector<Point2f> measurements);
  static bool objectIsLost(TrackObj obj)
    {  return (obj.framesUnseen() > 6);}
};


#endif
