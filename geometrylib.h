#include "opencv2/core/core.hpp"
using namespace cv;

double area2(Point2f a, Point2f b, Point2f c);
bool left (Point2f a,Point2f  b,Point2f c);
bool collinear(Point2f a, Point2f b, Point2f c);
bool intersects(Point2f a, Point2f b, Point2f c, Point2f d);
bool intersectsProp(Point2f a, Point2f b, Point2f c, Point2f d);
bool between(Point2f a, Point2f b, Point2f c);

